<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::resource('task', [TaskController::class]);
// Route::resource('category', [CategoryController::class]);
// Route::get('category/{category}/tasks', [CategoryController::class, 'tasks']);

Route::post('login', [UserController::class, 'login'])->name('login');
Route::post('register', 'UserController@register');

Route::group(['middleware' => 'auth:api'], function(){
    Route::resource('/task', TaskController::class);
    Route::resource('/category', CategoryController::class);
    Route::get('/category/{category}/tasks', 'CategoryController@tasks');
});
